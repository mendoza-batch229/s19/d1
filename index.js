// console.log("Hello AL ZAILO!");


// [SECTION] - if, else if, else statement

let numG = -1;

// IF STATEMENT
// Excecutes statement if  a specified condition is true

// This will not excute since the condition is false
if(numG < -5){
	console.log("Hello");
}
// This will execute since the condition is true
if(numG < 1){
	console.log("Hello");
}

// ELSE IF STATEMENT
/*
1. Executes statement  if a previous condition are false and if the condition specified is true.
2. The "else if" clause is optional and can be added to capture additional condition to change the flow of a program.
*/

let numH = 1;

if(numG > 0){
	console.log("Hello");
}else if (numH >0){
	console.log("World");
}

// ELSE STATEMENT
/*
1. Execute statement if all other conditions are false.
2. The "else" statement is optional and can be added to capture any other result  to change the flow of the program
*/

if (numG > 0) {
	console.log("Hello");
}else if (numH == 0){
	console.log("World");
}else{
	console.log("Again!");
}


// IF, ELSE IF and ELSE statement with functions

let message = "No Message";
console.log(message);

function determineTyphoonIntensity(windsSpeed){
	if(windsSpeed < 30){
		return "Not a Typhoon yet.";
	}else if(windsSpeed <= 61){
		return "Tropical Depression Detected."
	}else if (windsSpeed >= 62 && windsSpeed <= 88) {
		return "Tropical Storm Detected. Warning!";
	}else if (windsSpeed >= 89 && windsSpeed <= 117) {
		return "Severe Tropical Storm Detected";
	}else{
		return "Typhoon Detected";
	}
}

// Return the string to the variable "message" that invoked it.
message = determineTyphoonIntensity(70);
console.log(message);
if (message == "Tropical Storm Detected. Warning!") {
	console.warn(message);
}

message = determineTyphoonIntensity(112);
console.log(message);

message = determineTyphoonIntensity(120);
console.log(message);


// [SECTION] - TRUTHY and FALSY
/* 
    - In JavaScript a "truthy" value is a value that is considered true when encountered in a Boolean context
    - Values are considered true unless defined otherwise
    - Falsy values/exceptions for truthy:
        1. false
        2. 0
        3. -0
        4. ""
        5. null
        6. undefined
        7. NaN
*/

// Thruty Examples


if (true) {
	console.log("truthy");
}
if(1){
	console.log("truthy");
}
if([]){
	console.log("truthy");
}


// Falsy examples
if(false){
	console.log("truthy");
}

if(0){
	console.log("truthy");
}

if(undefined){
	console.log("truthy");
}


// [SECTION] - Conditional (Ternary) Operator or Conditional Rendering

/* 
    - The Conditional (Ternary) Operator takes in three operands:
        1. condition
        2. expression to execute if the condition is truthy
        3. expression to execute if the condition is falsy
    - Can be used as an alternative to an "if else" statement
    - Ternary operators have an implicit "return" statement meaning that without the "return" keyword, the resulting expressions can be stored in a variable
    - Commonly used for single statement execution where the result consists of only one line of code
    - For multiple lines of code/code blocks, a function may be defined then used in a ternary operator
    - Syntax
        (expression/condition) ? ifTrue : ifFalse;
*/

// Single statement execution
let ternaryResult = (1 < 18) ? true : false;
console.log("Result of tenary operator: " + ternaryResult);

let ternaryResult1 = (100 < 18) ? true : false;
console.log("Result of tenary operator: " + ternaryResult1);

// Multiple Statement Execution
// Both Functions perform two seperate task witch changes the value of the "name" cariable md returns the result storing it in the "legalAge" variable.

let name;

function isOfLegalAge(){
	name = "John";
	return "You are of the legal age limit"
}

function isOfUnderAge(){
	name= "Jane";
	return "You are under the age limit";
}

let age = parseInt(prompt("What is your age?"));
	console.log(typeof age);
	console.log(age);

let legalAge = (age > 18) ? isOfLegalAge() : isOfUnderAge();
	console.log("Result of ternary operator in function: " + legalAge + ", " + name);


// [SECTION] - Switch Statement
/* 
	- Can be used as an alternative to an if, "else if and else" statement where the data to be used in the condition is of an expected input
	- The ".toLowerCase()" function/method will change the input received from the prompt into all lowercase letters ensuring a match with the switch case conditions if the user inputs capitalized or uppercased letters
	- The "expression" is the information used to match the "value" provided in the switch cases
	- Variables are commonly used as expressions to allow varying user input to be used when comparing with switch case values
	- Switch cases are considered as "loops" meaning it will compare the "expression" with each of the case "values" until a match is found
	- The "break" statement is used to terminate the current loop once a match has been found
	- Removing the "break" statement will have the switch statement compare the expression with the values of succeeding cases even if a
	a match was found
	- Syntax
	switch (expression) {
	    case value:
	        statement;
	        break;
	    default:
	        statement;
	        break;
	}
*/

let day = prompt("What day of the week is it today?").toLowerCase();
	console.log(day);

switch (day){
	case 'sunday':
	console.log("The color of the day is violet");
		break;
	case 'monday':
	console.log("The color of the day is red");
		break;
	case 'monday':
	console.log("The color of the day is orange");
		break;
	case 'wednesday':
	console.log("The color of the day is yellow");
		break;
	case 'thursday':
	console.log("The color of the day is green");
		break;
	case 'friday':
	console.log("The color of the day is blue");
		break;
	case 'saturday':
	console.log("The color of the day is indigo");
		break;
	default:
	console.log("Please input a valid day");
		break;

}


// [SECTION] - Try-Catch-Finally Statement
function showIntensityAlert(windsSpeed){
	try{
		// try to attempt to execute a code
		alert(determineTyphoonIntensity(windsSpeed))
	}catch (error){
		console.log(typeof error);
		console.warn(error.message);
	}finally{
		alert("Intensity updates will show new alert");
	}
}
showIntensityAlert(56);